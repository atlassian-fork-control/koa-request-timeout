import { Config } from '@atlassian/node-config';
import { ioTsGuard } from '@atlassian/node-config-io-ts';
import * as t from 'io-ts';

// tslint:disable:variable-name
export const RequestTimeoutConfig = t.exact(
  t.type({
    timeoutHttpStatus: t.number,
    timeoutInMs: t.number
  })
);

export type RequestTimeoutConfig = t.TypeOf<typeof RequestTimeoutConfig>;
export interface KeyedRequestTimeoutConfig {
  requestTimeout: RequestTimeoutConfig;
}

export const requestTimeoutConfig = new Config<KeyedRequestTimeoutConfig>({
  defaults: {
    requestTimeout: {
      timeoutHttpStatus: 504,
      timeoutInMs: 2500
    }
  },
  guard: ioTsGuard<KeyedRequestTimeoutConfig>(RequestTimeoutConfig, 'requestTimeout')
});
