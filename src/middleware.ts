import { Context, Middleware } from 'koa';

import { RequestTimeoutConfig } from './config';

/**
 * Extends Koa's context with requestTimeout namespace
 */
declare module 'koa' {
  interface RequestTimeoutContext {
    hasTimedOut: boolean;
    timer?: NodeJS.Timer;
  }

  interface Context {
    requestTimeout?: RequestTimeoutContext;
  }
}

/**
 * Error thrown when request has timed
 *
 * @constructor
 * @param {number} status returned HTTP status
 * @param {string} message error message
 */
export class RequestTimeoutError extends Error {
  public readonly status: number;
  public readonly expose: boolean = false; // don't expose error message to clients

  constructor(status: number, message: string) {
    super(message);
    this.status = status;
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * Returns Promise rejecting after timeoutInMs
 *
 * @param {Context} ctx koa context
 * @param {number} timeoutInMs timeout in millisecs
 * @returns {Promise<void>}
 */
export const timeoutPromise = (ctx: Context, timeoutInMs: number) =>
  new Promise((resolve, reject) => {
    if (!ctx.requestTimeout) {
      return reject(new Error(`Missing request timeout context`));
    }

    ctx.requestTimeout.timer = setTimeout(() => {
      ctx.requestTimeout!.hasTimedOut = true;
      reject(new Error(`Request timed out after ${timeoutInMs} ms`));
    }, timeoutInMs);
  });

/**
 * Clears request timeout timer (if found) in koa context
 *
 * @param {Context} ctx koa contexts
 */
export function clearRequestTimeout(ctx: Context): void {
  if (ctx.requestTimeout && ctx.requestTimeout.timer) {
    clearTimeout(ctx.requestTimeout.timer);
  }
}

/**
 * Request timeout middleware factory
 *
 * @param {RequestTimeoutConfig} config middleware configuration
 * @returns {(Context, () => Promise<Middleware>) => Promise<void>} koa middleware
 */
export default function createRequestTimeoutMiddleware(
  config: RequestTimeoutConfig
): (ctx: Context, next: () => Promise<Middleware>) => Promise<void> {
  const { timeoutHttpStatus, timeoutInMs } = config;

  return async function requestTimeoutMiddleware(ctx: Context, next: () => Promise<Middleware>): Promise<void> {
    ctx.requestTimeout = { hasTimedOut: false };

    try {
      await Promise.race([next(), timeoutPromise(ctx, timeoutInMs)]);

      clearRequestTimeout(ctx);
    } catch (error) {
      if (ctx.requestTimeout.hasTimedOut) {
        throw new RequestTimeoutError(timeoutHttpStatus, error.message);
      }

      clearRequestTimeout(ctx);
      throw error;
    }
  };
}
